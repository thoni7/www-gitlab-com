---
layout: markdown_page
title: "Category Direction - Gitaly"
description: "Gitaly is a Git RPC service for handling all the Git calls made by GitLab. Find more information here!"
canonical_path: "/direction/create/gitaly/"
---

- TOC
{:toc}

## Gitaly

| Section | Stage | Maturity | Last Reviewed |
| --- | --- | --- | --- |
| [Dev](/direction/dev/) | [Create](https://about.gitlab.com/stages-devops-lifecycle/create/) | Non-marketable | 2021-01-26 |

## Introduction and how you can help

The Gitaly direction page belongs to the [Gitaly](/handbook/product/categories/#source-code-group)
group of the [Create](/direction/create/) stage,
and is maintained by [Mark Wood](https://gitlab.com/mjwood).

This strategy is a work in progress, and everyone can contribute.
Please comment and contribute in the linked issues and epics.
Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Agitaly)
- [Epic list](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=group%3A%3Agitaly)

## Overview

<!--
A good description of what your category is.
If there are special considerations for your strategy or how you plan to prioritize, the description is a great place to include it.
Please include usecases, personas, and user journeys into this section.
-->

Gitaly is the service responsible for the storage and maintenance of all Git repositories in GitLab.
Git repositories are essential to GitLab, for [Source Code Management](/direction/create/source_code_management/), [Wikis](/direction/create/editor/wiki/), [Snippets](/direction/create/editor/snippets/), Design Management, [Web IDE](/direction/create/editor/web_ide/), and every stage to the DevOps lifecycle to the right of Create - Verify, Release, Package, Release, Configure, Monitor, Secure, and Protect - depend on the project repositories. Because the majority of GitLab capabilities depend on that information stored in Git repositories, performance and availability are of primary importance.

GitLab is used to store Git repositories by small teams and large enterprises with many terabytes of data. For this reason, Gitaly has been built to scale from small single server GitLab instances, to large high availability architectures. The [recent release of Gitaly Cluster](/releases/2020/05/22/gitlab-13-0-released/#gitaly-cluster-for-high-availability-git-storage) is a major achievement in improving fault tolerance and performance, and is the foundation on which we are continuing to iterate to improve Gitaly for large instances.

Continued investment in large software projects over many years can result in extremely large Git repositories. Contributing to the development of features like partial clone in Git, and improving Gitaly and GitLab for these enterprise scale repositories is an ongoing area of investment.

Gitaly provides multiple interface to read and write Git data:

- Git protocol over SSH, through the GitLab Shell component.
- Git protocol over HTTP, through the GitLab Workhorse component.
- gRPC internally to GitLab components. The public REST and GraphQL APIs to Git data are implemented using these RPCs.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/RNLWrvIkB9E" frameborder="0" allowfullscreen="true"></iframe>
</figure>
<!-- blank line -->

### Target Audience

<!--
An overview of the personas involved in this category.
An overview of the evolving user journeys as the category progresses through minimal, viable, complete and lovable maturity levels.
-->

**Systems Administrators** directly interact with Gitaly when installing, configuring, and managing a GitLab server, particularly when high availability is a requirement. Today systems administrator must create and manage an NFS cluster to configure a high availability GitLab instance, and manual manage the failover to new Gitaly nodes mounted on the same NFS cluster. Once a HA Gitaly reaches minimal viability, it will be possible to eliminate the NFS cluster from architecture and rely on Gitaly for replication. At HA Gitaly continues to mature, automatic failover, automatic Gitaly node rebalancing and horizontal scaling read access across replicas will deliver 99.999% uptime (five 9's) and improved performance without regular intervention. Systems Administrators will have fewer applications to manage as other version control systems are retired as the last projects are migrated to GitLab.

**Developers** will benefit from increasing performance for repositories of all shapes and sizes, on the command line and in the GitLab application as performance improvements continue. Once support for monolithic repositories reaches minimal and continues maturing, developers will no longer be split between Git and legacy version control systems, as projects consolidate increasingly on Git. Developers that heavily use binary assets, like **Game Developers**, will at long last be able to switch to Git and eliminate Git LFS by adopting native large file support in Git.

<!--
### Challenges to address

- What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist.
-->

## Where we are Headed

<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this category once your strategy is at least minimally realized.
-->

The performance and availability of Gitaly is matter of importance for GitLab Administrators. The inability to access Git repositories on a GitLab server is an outage event, and for a large instance would prevent thousands of people from doing their job. The [recent release of Gitaly Cluster](/releases/2020/05/22/gitlab-13-0-released/#gitaly-cluster-for-high-availability-git-storage) is a major achievement in improving fault tolerance and performance. Continued iteration is need to further improve fault tolerance, performance, and complete roll out to GitLab.com.

- [Gitaly Clusters: strong consistency](https://gitlab.com/groups/gitlab-org/-/epics/1189)
- [Elastic Gitaly Clusters](https://gitlab.com/groups/gitlab-org/-/epics/3372)

Git is the market leading Version Control System (VCS), but many organizations with extremely large projects continue to use centralized version control systems like CVS, SVN, and Perforce. These organizations have often widely adopted Git, but isolated large legacy repositories remain elsewhere. Improvements to Git like partial clone and spare checkout, to Gitaly, GitLab will make it possible to standardize on Git for extremely large repositories, and allow organizations to consolidate on Git.

- [Git for enormous repositories](https://gitlab.com/groups/gitlab-org/-/epics/773)

### What's Next & Why

<!--
This is almost always sourced from the following sections, which describe top priorities for a few stakeholders.
This section must provide a link to an issue or [epic](/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.
-->

- **In progress:** [Gitaly Clusters: strong consistency](https://gitlab.com/groups/gitlab-org/-/epics/1189)

    When a developer pushes changes to GitLab, if a success signal is returned,
    GitLab should have more than one copy of this data to prevent data loss.
    Strong consistency is the highest priority after shipping the eventually
    consistent MVC.

- **In progress:** [Incremental repository backups](https://gitlab.com/groups/gitlab-org/-/epics/2094)

    Backing up GitLab instances with massive amounts of Git data is slow and
    difficult. The built-in backup script can't cope with large amounts of Git
    data. Workarounds include disk snapshots and rsync, and even then backups
    may take many many hours, and are likely to be inconsistent. A frequent
    request is point in time recovery from backups.

### What is Not Planned Right Now

<!--
Often it's just as important to talk about what you're not doing as it is to discuss what you are.
This section should include items that people might hope or think we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should in fact do.
We should limit this to a few items that are at a high enough level so someone with not a lot of detailed information about the product can understand the reasoning.
-->

- [VFS for Git](https://gitlab.com/groups/gitlab-org/-/epics/93)

    Partial Clone is built-in to Git and available in GitLab 13.0 or newer.
    [Scalar](https://devblogs.microsoft.com/devops/introducing-scalar/) is
    compatible with partial clone, and Microsoft is contributing to its
    improvement based on their learnings from the GVFS protocol.

- Divergent solution for CDN Offloading

    While we recognize that a lot of good work has gone into independent
    solutions, we are committed to work with the Git community on a CDN
    approach. We intend to support, implement, and contribute to this 
    solution as it be comes available. This is currently being explored
    in our [Support Git CDN offloading](https://gitlab.com/groups/gitlab-org/-/epics/1692)
    epic.

### Maturity Plan

<!--
It's important your users know where you're headed next.
The maturity plan section captures this by showing what's required to achieve the next level.
-->

Gitaly is a **non-marketable** category, and is therefore not assigned a maturity level.

## Competitive Landscape

<!--
Lost the top two or three competitors.
What the next one or two items we should work on to displace the competitor at customers?
Ideally these should be discovered through [customer meetings](/handbook/product/product-processes/#customer-meetings).

We’re not aiming for feature parity with competitors,
and we’re not just looking at the features competitors talk about,
but we’re talking with customers about what they actually use,
and ultimately what they need.
-->

Important competitors are [GitHub.com](https://github.com) and [Perforce](https://perforce.com) which, in relation to Gitaly, compete with GitLab in terms of raw Git performance and support for enormous repositories respectively.

Customers and prospects evaluating GitLab (GitLab.com and self hosted) benchmark GitLab's performance against GitHub.com, including Git performance. The Git performance of GitLab.com for easily benchmarked operations like cloning, fetching and pushing, show that GitLab.com similar to GitHub.com.

Perforce competes with GitLab primarily on its ability to support enormous repositories, either from binary files or monolithic repositories with extremely large numbers of files and history. This competitive advantage comes naturally from its centralized design which means only the files immediately needed by the user are downloaded. Given sufficient support in Git for partial clone, and sufficient performance in GitLab for enormous repositories, existing customers are waiting to migrate to GitLab.

- [Git for enormous repositories](https://gitlab.com/groups/gitlab-org/-/epics/773)

## Business Opportunity

<!--
This section should highlight the business opportunity highlighted by the particular category.
-->

The version control systems market is expected to be valued at close to US$550mn in the year 2021 and is estimated to reach US$971.8md by 2027 according to [Future Market Insights](https://www.futuremarketinsights.com/reports/version-control-systems-market) which is broadly consistent with revenue estimates of GitHub ([$250mn ARR](https://www.owler.com/company/github)) and Perforce ([$130mn ARR](https://www.owler.com/company/perforce)). The opportunity for GitLab to grow with the market, and grow it's share of the version control market is significant.

Git is the market leading version control system, demonstrated by the [2018 Stack Overflow Developer Survey](https://insights.stackoverflow.com/survey/2018/#work-_-version-control) where over 88% of respondents use Git. Although there are alternatives to Git, Git remains dominant in open source software, usage by developers continues to grow, it installed by default on macOS and Linux, and the project itself continues to adapt to meet the needs of larger projects and enterprise customers who are adopting Git, like the Microsoft Windows project.

According to a [2016 Bitrise survey](https://blog.bitrise.io/state-of-app-development-2016#self-hosted) of mobile app developers, 62% of apps hosted by SaaS provider were hosted in GitHub, and 95% of apps are hosted in by a SaaS provider. These numbers provide an incomplete view of the industry, but broadly represent the large opportunity for growth in SaaS hosting on GitLab.com, and in self hosted where GitLab is already very successful.

## Analyst Landscape

<!--
What are analysts and/or thought leaders in the space talking about?
What are one or two issues that will help us stay relevant from their perspective?
-->

- [Native support for large files](https://gitlab.com/groups/gitlab-org/-/epics/773) is important to companies that need to version large binary assets, like game studios. These companies primarily use Perforce because Git LFS provides poor experience with complex commands and careful workflows needed to avoid large files entering the repository. GitLab has been supporting work to provide a more native large file workflow based on promiser packfiles which will be very significant to analysts and customers when the feature is ready.

## Top Customer Success/Sales issue(s)

<!--
These can be sourced from the CS/Sales top issue labels when available,
internal surveys, or from your conversations with them.
-->

- [Gitaly Clusters: strong consistency](https://gitlab.com/groups/gitlab-org/-/epics/1189) will provide improved fault tolerance by guaranteeing a quorum of Gitaly nodes have accepted write operations before reporting a success to the client. This will make automatic fail possible with a high degree of confidence that no data loss will occur.
- [Incremental repository backups](https://gitlab.com/groups/gitlab-org/-/epics/2094) will provide a comprehensive backup solution for GitLab instances with large amounts of Git data.
- [Native support for extremely large repositories](https://gitlab.com/groups/gitlab-org/-/epics/773) prevents existing customers and prospects from being able to migrate enormous repositories from Perforce or SVN to Git. It is frequently requested and many organizations want to standardize on a single version control system and tool like GitLab across all projects.

## Top user issue(s)

<!--
This is probably the top popular issue from the category (i.e. the one with the most thumbs-up),
but you may have a different item coming out of customer calls.
-->

Users do not see Gitaly as a distinct feature or interface of GitLab.
Git performance is the most significant user facing area where improvements are frequently requested,
however the source of the performance problem can vary significantly.

## Top internal customer issue(s)

<!--
These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding) the product.
-->

- [Gitaly Clusters: strong consistency](https://gitlab.com/groups/gitlab-org/-/epics/1189) will provide improved fault tolerance by guaranteeing a quorum of Gitaly nodes have accepted write operations before reporting a success to the client. This will make automatic fail possible with a high degree of confidence that no data loss will occur.
- [Streaming incremental Git backups](https://gitlab.com/groups/gitlab-org/-/epics/2094) will replace inconsistent disk snapshots for GitLab.com.

## Top Vision Item(s)

<!--
What's the most important thing to move your vision forward?
-->

- [Gitaly Clusters: strong consistency](https://gitlab.com/groups/gitlab-org/-/epics/1189) will provide improved fault tolerance by guaranteeing a quorum of Gitaly nodes have accepted write operations before reporting a success to the client. This will make automatic fail possible with a high degree of confidence that no data loss will occur.
- [Native support for large files](https://gitlab.com/groups/gitlab-org/-/epics/773) prevents existing customers and prospects being able to migrate repositories with large files to Git. Git LFS isn't a sufficient solution for these organisations in comparison with the native support of other version control systems. The most pressing problem is avoiding the need to download enormous amounts of data, and not having to remember to use different commands for different files so as not to make life worse for everyone.
