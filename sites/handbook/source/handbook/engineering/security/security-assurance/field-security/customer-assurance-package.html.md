---
layout: handbook-page-toc
title: Customer Assurance Package Internal Request Process
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# GitLab's Customer Assurance Package

Are you looking for GitLab's Customer Assurance Package? We've moved! Please click below to be redirected:

<div class="flex-row" markdown="0" style="height:80px">
    <a href="https://about.gitlab.com/security/cap/" class="btn btn-purple-inv" style="width:100%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">GitLab Customer Assurance Package (Click Here!)</a>
</div> 

### CAP Request Process for GitLab Team Members

Due to the nature of some of GitLab's security certifications and reports, the Customer CAP is availble under a Non Discolsure Agreement or confidentiality contractual clauses only. 

These resources can be requested on behalf of your customer using the [Customer Assurance Activities](https://about.gitlab.comhandbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html) workflow or by emailing security@gitlab.com. 

## Additional Support

If you have any further questions, please follow the below steps:

**Prospective Customers**: Please [fill out a request](https://about.gitlab.com/sales/) and a representative will reach out to you.

**Current Customers**: Please contact your [Account Owner](/handbook/sales/#initial-account-owner---based-on-segment) at GitLab. If you don't know who that is, please [reach out to sales](https://about.gitlab.com/sales/) and ask to be connected to your Account Owner.

**GitLab Team Members**: Contact the Risk and Field Security team using the **Customer Assurance** workflow in the slack [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70) channel. 
