---
layout: job_family_page
title: "Product Analyst"
---

Product Analysts at GitLab are curious, highly strategic, and focused on making GitLab better through trusted data insights. Working with product teams, they look at customer behaviors across the customer journey and help make the customer experience and business outcomes better. As a member of the product data analysis team, the successful candidate will develop BI solutions to understand usage activity, create and own multiple Product Key Performance Indicators, develop a deep understanding of product health and the customer experience, advance data acumen across the company, and promote strategic decisions through data storytelling.

## Levels
There are levels below in this job family. 

### Product Analyst (Intermediate)
This role reports to the Director of Product, Growth as part of the Product team, with close partnership with the Data Team. 

#### Job Grade

This role is a [Job Grade](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) Level 6.

#### Responsibilities
- Perform strategic and ad-hoc data work in support of Product Managers
- Identify opportunities in data to accelerate product adoption, retention, engagement, and/or monetization 
- Develop dashboards and define metrics that inform success for the Product Team
- Help design, execute and evaluate A/B tests to improve the user journey
- Explore large, complex, and loosely defined datasets to create actionable insights
- Serve as the Technical DRI for the Product Key Performance Indicators
- Serve as the Business DRI for the Product areas of the Enterprise Dimensional Model -- the future of Trusted Data @ GitLab
- Explain trends across data sources, potential opportunities for growth or improvement, and data caveats for descriptive, diagnostic, predictive (including forecasting), and prescriptive data projects
- Develop user archetypes and build dashboards to demonstrate their usage patterns
- Capture and document data user stories, use cases, and workflows
- Craft code that meets our internal standards for style, maintainability, and best practices
- Operate in an iterative mindset, focused on delivering improvements and value at high velocity
- Document every action in either issue/MR templates, the handbook, or READMEs so your learnings turn into repeatable actions and then into automation following the GitLab tradition of handbook first!
- Work collaboratively with product team members such as: product managers, engineering, design, as well as with core data team members such as DA and data engineers

#### Requirements
- 3+ years of experience, leveraging descriptive and predictive data techniques
- 2+ years of experience focused on feature and usage metrics, A/B testing, conversion and retention, segmentation, and developing user archetypes and their usage patterns
- 1+ years of experience uncovering data insights to form hypothesis,  designing product experiments,  evaluating results and making recommendation
- Advanced Level SQL
- Advanced BI dashboard development (we use SiSense)
- Experience working with a Data Warehouse built using Kimball dimensional modeling
- Ability to design, document, and communicate models using Entity Relationship Diagrams 
- Able to read Ruby to trace data flows and code paths
- Comfort working in a highly agile, intensely iterative software development process
- Effective communication skills: Regularly achieve consensus with peers, and clear status updates
- Experience owning a project from concept to production, including proposal, discussion, and execution
- Self-motivated and self-managing, with strong organizational skills
- Share our values, and work in accordance with those values
- Ability to thrive in a fully remote organization, being collaborative and supportive. 
- Ability to use GitLab
- Previous experience supporting product or growth team
- Comfortable with fast-pace and  ambiguity in a start-up environment

### Senior Product Analyst

#### Job Grade
This role is a [grade 7](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Requirements
Similar to Product Analyst, with the experience requirements adjusted to:
- 6+ years of experience, leveraging descriptive and predictive data techniques
- 4+ years of experience focused on feature and usage metrics, A/B testing, conversion and retention, segmentation, and developing user archetypes and their usage patterns
- 2+ years of experience uncovering data insights to form hypothesis,  designing product experiments,  evaluating results and making recommendation 

### Manager, Product Data Analysis

#### Job Grade
This role is a [grade 8](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Requirements
All of the requirements of a Senior Product Analyst, plus relevant management experience:
- 2+ years of experience managing and developing a data analytics team
- 2+ years of experience responsible for planning, solutioning, work allocation & prioritization, stakeholder management and cross-functional collaboration as a data team lead

#### Responsibilities

This manager is expected to manage a small team of ICs, while also contributing in the Senior Product Analyst capacity, as described above. 
- Manage and lead a high performing team of ICs, including day-to-day assignments, weekly 1-1s, bi-weekly milestone planning, quarterly reviews, and annual objectives 
- Create efficient process to prioritize data requests and maximize the output of the team
- Proactively identify strategic opportunities for product team via data analysis 
- Collaborate with central data team to improve data infrastructure & process
- Help create a leading product Data Program to support GitLab's vision and advocate a data-driven culture 

## Performance Indicators
- Number of work delivered weekly
- [TMAU]/[SMAU](https://about.gitlab.com/handbook/product/performance-indicators/#stage-monthly-active-users-smau)
- [SPU](https://about.gitlab.com/handbook/product/performance-indicators/#stages-per-user-spu)
- [Direct Signup ARR Growth Rate](https://about.gitlab.com/handbook/product/performance-indicators/#direct-signup-arr-growth-rate)
- [Free to Paid ARR Growth Rate](https://about.gitlab.com/handbook/product/performance-indicators/#free-to-paid-arr-growth-rate)
- [ Net retention rate](https://about.gitlab.com/handbook/sales/performance-indicators/#net-retention) 
- [Gross retention rate](https://about.gitlab.com/handbook/sales/performance-indicators/#gross-retention)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

- Selected candidates will be invited to schedule a 30 min. [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters
- Next, candidates will be invited to schedule a first interview with the a Data team Leader.
- Next, candidates will be invited to interview with 1-4 teammates, including hiring manager.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
