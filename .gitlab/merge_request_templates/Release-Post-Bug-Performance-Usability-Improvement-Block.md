/label ~"release post" ~"release post item" ~"Technical Writing"

The Release Post Manager will use this template to create separate Bug Fixes, Performance Improvements, and Usability Improvements MRs for the release post blog.

[Instructions for MR](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-mrs-for-usability-improvements-bugs-and-performance-improvements)

## Key dates & Review

- [ ] By the 10th, `@Release Post Manager` informs EMs/PMs/PDs to draft/submit Usability improvements, Bugs, or Performance improvements via this MR per [release post MR task list item](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/merge_request_templates/Release-Post.md#release-post-item-creation-reminders-release_post_manager)
- [ ] By the 15th, `@Release Post Manager` assigns MR to `@TW Lead` for review and applies label `in review`
     - If there are no usability improvements yet added, alert UX DRI `@vkarnes` in this MR
     - If there are no bug fixes or performance improvements yet added, alert Product Operations DRI `@fseifoddini` in this MR 
- [ ] By the 16th: `@TW Lead` reviews, applies the `ready` label and assigns  to `@Release Post Manager`
- [ ] By the 17th: `@Release Post Manager` merges the MR, prior to final content assembly
